# SketchedLearning SPM notebooks
This repository provides demo Jupyter notebooks to accompany the following review paper on compressive learning:

**Sketching Datasets for Large-Scale Learning**<br>
Rémi Gribonval, Antoine Chatalic, Nicolas Keriven, Vincent Schellekens, Laurent Jacques, and Philip Schniter<br>
[arXiv:2008.01839](https://arxiv.org/abs/2008.01839)

The repository contains a few illustrative toy examples:
1. "An invitation to compressive learning", which shows on a very-large-scale dataset how compressive learning saves computational resources
2. "Building intuition for compressive learning", an interactive notebook to play around with to understand why compressive learning works (vizualization of the cost function, etc.) 
3. "Quantization aspects", where we illustrate how sketch contribution can be quantized at essentially no cost (this notebook is also a bit interactive)

The code for sketching (and subsequent learning) is based on the `pycle` toolbox for compressive learning, whose latest version is available [here](https://github.com/schellekensv/pycle)  (also available on the [Python package index](https://pypi.org/project/pycle/)).

The notebooks (and the `pycle` toolbox) are under the MIT license (see `LICENSE`).



### Requirements
- python 3
- matplotlib
- scipy and numpy

For the interactive notebooks, you'll also need:
- ipywidgets
- ipympl