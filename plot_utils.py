import numpy as np
import scipy.stats
from matplotlib import cm

def do_the_plot_for_notebook_2(task,d,ax1,ax2,ax3,thetas_0,thetas_1,X,cost_L,cost_R,ballsize,cR,cL,markersize,lw,pos_fine):
    """Plotting utility for the second notebook, creates three subplots ax1, ax2 and ax3 according to the given task."""
    # Find minimum of both costs
    index_sol_L = np.unravel_index(np.argmin(cost_L),cost_L.shape)
    index_sol_R = np.unravel_index(np.argmin(cost_R),cost_R.shape)
    index_sol_R = tuple(np.flip(index_sol_R))
    index_sol_L = tuple(np.flip(index_sol_L))
    
    # First plot (data)
    ax1.cla()
    if task == '1means2D':
        ax1.scatter(X[:,0],X[:,1],s=1, alpha=0.1)
        ax1.scatter(thetas_0[index_sol_R],thetas_1[index_sol_R], s=ballsize+8, c=cR, label="true params.")
        ax1.scatter(thetas_0[index_sol_L],thetas_1[index_sol_L], s=ballsize-6, c=cL, label="sketched params.")
        ax1.set_ylim((-1,1))
        ax1.set_xlabel(r'$x_1$')
        ax1.set_ylabel(r'$x_2$')
    elif task == '2means1D':
        ax1.hist(X,bins=40, weights = np.ones(X.shape[0])/X.shape[0], alpha=0.55)
        ylim = ax1.get_ylim()
        ax1.plot(thetas_0[index_sol_R],ylim[1]/20, marker=".",markersize=markersize, c=cR, label="true params.")
        ax1.plot(thetas_1[index_sol_R],ylim[1]/20, marker=".",markersize=markersize, c=cR)
        ax1.plot(thetas_0[index_sol_L],ylim[1]/20, marker=".",markersize=markersize-5, c=cL, label="sketched params.")
        ax1.plot(thetas_1[index_sol_L],ylim[1]/20, marker=".",markersize=markersize-5, c=cL)   
        ax1.set_xlabel(r'$x_1$')
    elif task == '1GMM1D':
        ax1.hist(X,bins=40, density=True, alpha=0.55)
        ax1.plot(pos_fine,scipy.stats.norm.pdf(pos_fine, loc = thetas_0[index_sol_R],
                                               scale=np.sqrt(10**thetas_1[index_sol_R])),c=cR,lw=lw+1.5, label="true params.")
        ax1.plot(pos_fine,scipy.stats.norm.pdf(pos_fine, loc = thetas_0[index_sol_L],
                                               scale=np.sqrt(10**thetas_1[index_sol_L])),c=cL,lw=lw, label="sketched params.")

        ax1.set_xlabel(r'$x_1$')
    ax1.set_xlim((-1, +1))
    ax1.set_title("Dataset ({}-dimensional)".format(d))
    ax1.legend()


    # Second plot (true cost)
    ax2.cla()
    cost_R_to_disp = np.log(cost_R - np.min(cost_R) + 1).T
    ax2.contourf(thetas_0,thetas_1,cost_R_to_disp,30)
    ax2.scatter(thetas_0[index_sol_R],thetas_1[index_sol_R], s=ballsize, c=cR)
    ax2.set_title("True risk $R$ (log scale)")
    ax2.set_xlabel(r'$\theta_1$')
    ax2.set_ylabel(r'$\theta_2$')
    if task == '1GMM1D':
        old_ticks = ax2.get_yticks()
        ax2.set_yticklabels(['$10^{'+str(t)+'}$' for t in old_ticks])

    # Second plot (sketched cost)
    ax3.cla()
    ax3.contourf(thetas_0,thetas_1,cost_L.T,30)
    ax3.scatter(thetas_0[index_sol_L],thetas_1[index_sol_L], s=ballsize, c=cL)
    ax3.set_title("Sketched cost $L$")
    ax3.set_xlabel(r'$\theta_1$')
    ax3.set_ylabel(r'$\theta_2$')
    if task == '1GMM1D':
        old_ticks = ax3.get_yticks()
        ax3.set_yticklabels(['$10^{'+str(t)+'}$' for t in old_ticks])
        
        
        
def do_the_plot_for_notebook_3(ax1,ax2,ax3,thetas_0,thetas_1,X,cost_EE,cost_EQ,cost_QQ,C_EE,C_EQ,s,ballsize,w_EE,w_EQ,cR,cL,nLines):
    """Plotting utility for the third notebook, creates three subplots ax1, ax2 and ax3 showing quantization effects."""
    # First plot (full-precision cost)
    ax1.cla()
    ax1.contourf(thetas_0,thetas_1,cost_EE.T,nLines,cmap=cm.Greens_r)
    ax1.scatter(X[:,0],X[:,1], color=cR, s=s)
    ax1.scatter(C_EE[:,0],C_EE[:,1], s=ballsize*w_EE, c=cL)
    ax1.set_title("Full-precision cost")
    ax1.set_xlabel(r'$\theta_1$')
    ax1.set_ylabel(r'$\theta_2$')
    
    # Second plot (true cost)
    ax2.cla()
    ax2.contourf(thetas_0,thetas_1,cost_EQ.T,nLines,cmap=cm.Greens_r)
    ax2.scatter(X[:,0],X[:,1], color=cR, s=s)
    ax2.scatter(C_EQ[:,0],C_EQ[:,1], s=ballsize*w_EQ, c=cL)
    ax2.set_title("Semi-quantized cost")
    ax2.set_xlabel(r'$\theta_1$')
    ax2.set_ylabel(r'$\theta_2$')

    # Second plot (sketched cost)
    ax3.cla()
    ax3.contourf(thetas_0,thetas_1,cost_QQ.T,nLines,cmap=cm.Greens_r)
    ax3.scatter(X[:,0],X[:,1], color=cR, s=s)
    ax3.set_title("Fully quantized cost")
    ax3.set_xlabel(r'$\theta_1$')
    ax3.set_ylabel(r'$\theta_2$')